﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    private Rigidbody2D _ridgidbody2D;
    private SpriteRenderer _spriteRenderer;
    private float _moveX = 0f;
    private float _speed = 17.5f;
    private const float BorderPosition = 5.15f;

    private void Awake()
    {
        _ridgidbody2D = GetComponent<Rigidbody2D>();
       _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
        float positionX = _ridgidbody2D.position.x + _moveX * _speed * Time.fixedDeltaTime;
        positionX = Mathf.Clamp(positionX, - BorderPosition + (_spriteRenderer.size.x / 2), BorderPosition - (_spriteRenderer.size.x / 2));
        _ridgidbody2D.MovePosition(new Vector2(positionX, _ridgidbody2D.position.y));
    }

    private void OnEnable()
    {
        PlayersInput.OnMove += Move;
    }

    private void OnDisable()
    {
        PlayersInput.OnMove += Move;
    }

    private void Move(float moveX)
    {
        _moveX = moveX;
    }



}
