﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class LevelButton : MonoBehaviour
{
    [SerializeField] private Button _button;
    [SerializeField] private Text _buttoText;
    [SerializeField] private Image _starImage;
    [SerializeField] private Sprite[] _starSprites;
    public int _index;

    public void SetData(Progress progress,int index)
    {
        _buttoText.text = (index+1).ToString();
        _index = index;
        _button.interactable = progress.IsOpened;
        _starImage.sprite = _starSprites[progress.StarsCount];
    }

    public void LevelSelected()
    {
        LoadingScreen.Screen.Enable(true);
        levelIndex levelIndex = new levelIndex();
        levelIndex.SetIndex(_index);
        Loader loader = new Loader();
        loader.LoadingMainScene(false);
    }
}
