﻿using UnityEngine;

public class BallCollisions : MonoBehaviour
{
    [SerializeField] private BallMove _ball;
    private float _lastPositionX;

    private void OnCollisionEnter2D(Collision2D collision)
    {

        //_ball.MoveCollision(collision);

          
        //  if(collision.gameObject.TryGetComponent(out PlayerMove player))
        // {
        //   if(ballPositionX < _lastPositionX + 0.1 && ballPositionX > _lastPositionX - 0.1)
        //   {
        //      float collisionPointX = collision.contacts[0].point.x;
        //      _rigidbody2D.velocity = Vector2.zero;
        //      float playerCenterPosition = player.gameObject.GetComponent<Transform>().position.x;
        //     float difference = playerCenterPosition - collisionPointX;
        //     float direction = collisionPointX < playerCenterPosition ? -1 : 1;
        //     _rigidbody2D.AddForce(new Vector2(direction * Mathf.Abs(difference * (Force / 2)), Force));
        // }
        //}

         float ballPositionX = transform.position.x;

        if (collision.gameObject.TryGetComponent(out PlayerMove playerMove))
        {
            if (ballPositionX < _lastPositionX + 0.1 && ballPositionX > _lastPositionX - 0.1)
            {
                float collisionPointX = collision.contacts[0].point.x;
                float playerCenterPosition = playerMove.gameObject.transform.position.x;
                float difference = playerCenterPosition - collisionPointX;
                float direction = collisionPointX < playerCenterPosition ? -1 : 1;
                _ball.AddForce(direction * Mathf.Abs(difference));
            }
        }
         _lastPositionX = ballPositionX;

        if (collision.gameObject.TryGetComponent(out IDamageable damageable))
        {
            damageable.ApplyDamage();
        }
    }
}
