﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private bool _isActiv;
    private const float Force = 300f;
    


    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
    }

    private void OnEnable()
    {
        PlayersInput.OnClicked += BallActivete;
    }

    private void OnDisable()
    {
        PlayersInput.OnClicked -= BallActivete;
    }




    private void BallActivete()
    {
        if (!_isActiv)
        {

        
          _isActiv = true;
          transform.SetParent(null);
          _rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
        
          AddForce();
        }
    }
    
    public void AddForce(float direction=0f)
    {
        _rigidbody2D.velocity = Vector2.zero;
        _rigidbody2D.AddForce(new Vector2(direction * (Force / 2), Force));
    }
}
