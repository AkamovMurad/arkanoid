﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowEnent : MonoBehaviour
{
    [SerializeField] private GameState _gameState;
    [SerializeField] private GameObject _pauseButton;

    public void OnEnable()
    {
        _pauseButton.SetActive(false);
        _gameState.SetState(State.StopGame);
    }
    public void OnDisable()
    {
        _pauseButton.SetActive(true);
    }
}
