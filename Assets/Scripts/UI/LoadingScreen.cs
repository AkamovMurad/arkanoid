﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{
    public static LoadingScreen Screen = null;
    [SerializeField] Canvas _canvas;

    private void Awake()
    {
        
        if(Screen == null)
        {
            Screen = this;
            DontDestroyOnLoad(gameObject);
            
        }
        else
        {
            Destroy(gameObject);
        }

    }
    public void Enable(bool value)
    {
       _canvas.enabled = value;
    }
    
}
