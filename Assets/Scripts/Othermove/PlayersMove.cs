﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersMove : MonoBehaviour
{
    private Rigidbody2D rb2D;
    void Awake()
    {
       
        rb2D = gameObject.AddComponent<Rigidbody2D>();
    }
    private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            rb2D.MovePosition(touch.position * Time.fixedDeltaTime);
        }

    }
    
}
