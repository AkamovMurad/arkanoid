﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
	Camera cam;
	Rigidbody2D rb;

	[SerializeField] HingeJoint2D[] wheels;
	JointMotor2D motor;

	[SerializeField] float CannonSpeed;
	bool isMoving = false;

	Vector2 pos;
	float screenBounds;
	float velocityX;

	void Start()
	{
		cam = Camera.main;

		rb = GetComponent<Rigidbody2D>();
		pos = rb.position;

		motor = wheels[0].motor;

		
	}

	void Update()
	{
		//Check player input ( hand or mouse drag)
		isMoving = Input.GetMouseButton(0);

		if (isMoving)
		{
			pos.x = cam.ScreenToWorldPoint(Input.mousePosition).x;
		}
	}

	void FixedUpdate()
	{
		//Move the cannon
		if (isMoving)
		{
			rb.MovePosition(Vector2.Lerp(rb.position, pos, CannonSpeed));
		}
		else
		{
			rb.velocity = Vector2.zero;
		}
	}
}