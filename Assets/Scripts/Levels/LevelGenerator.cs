﻿using UnityEngine;

public class LevelGenerator : MonoBehaviour
{

    private readonly levelIndex _levelIndex = new levelIndex();
    private readonly BlocksGenerator _blocksGenerator = new BlocksGenerator();
    [SerializeField] private Transform _parentBlocks;
    [SerializeField] private ClearLevel _clearLevel;
    private void Start()
    {
        Init();
    }

    private void Init()
    {
        _clearLevel.Clear();
        GameLevel gameLevel = Resources.Load<GameLevel>($"Levels/level{_levelIndex.GetIndex()}");
        if (gameLevel != null)
        {
            _blocksGenerator.Generate(gameLevel, _parentBlocks);
        }
        LoadingScreen.Screen.Enable(false);

    }
    public void Generate()
    {
        LoadingScreen.Screen.Enable(true);
        Init();
    }

}
